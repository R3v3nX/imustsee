//
//  ImdbCommunicationDelegateTests.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 23.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OmdbCommunicationDelegate.h"

@interface OmdbCommunicationDelegateTests : XCTestCase

@property OmdbCommunicationDelegate* imdbDelegate;

@end

@implementation OmdbCommunicationDelegateTests

- (void)setUp
{
    [super setUp];
    _imdbDelegate = [[OmdbCommunicationDelegate alloc] init];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testShouldSendRequestToImdbDatabaseWithNoError
{
    XCTestExpectation* expectation = [self expectationWithDescription:@"Get data from Imdb"];
    [_imdbDelegate send: @"Star+Wars"
                   completionHandler: ^(NSData* data, NSURLResponse* response, NSError* error)
    {
        XCTAssertNil(error);
        XCTAssertTrue([data length] > 0);
        
        [expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:5.0 handler: nil];
}

- (void)testShouldSendRequestToImdbDatabaseWithError
{
    XCTestExpectation* expectation = [self expectationWithDescription:@"Get no data from Imdb"];
    
    [_imdbDelegate send: @" "
                   completionHandler: ^(NSData* data, NSURLResponse* response, NSError* error)
     {
         XCTAssertNotNil(error);
         XCTAssertTrue([data length] == 0);
         
         [expectation fulfill];
     }];

    
    [self waitForExpectationsWithTimeout:5.0 handler: nil];
}

@end
