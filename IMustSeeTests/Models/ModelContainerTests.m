//
//  FilmManagerTests.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 06.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ModelContainer.h"
#import "Model.h"

@interface ModelContainerTests : XCTestCase

@property ModelContainer* modelContainer;

@end

@implementation ModelContainerTests

-(void) setUp
{
    [super setUp];
    _modelContainer = [[ModelContainer alloc] init];
}

-(void) tearDown
{
    [super tearDown];
}

-(void) testAddObject
{
    [_modelContainer addObject: [[Model alloc] init]];
    
    XCTAssertEqual([_modelContainer count], 1);
    
}

-(void) testAddOnlyNewContent
{
    [_modelContainer addObject: [[Model alloc] init]];
    [_modelContainer addObject: [[Model alloc] init]];
    
    XCTAssertEqual([_modelContainer count], 1);
}

-(void) testReturnEmptyFilmWhenIndexIsOutOfRange
{
    Model* model = [_modelContainer objectAtIndex: 3];
    
    XCTAssertNil(model);
}

-(void) testRemoveContent
{
    [_modelContainer addObject: [[Model alloc] init]];
    [_modelContainer removeObjectAtIndex: 0];
    [_modelContainer removeObjectAtIndex: 0];
    
    XCTAssertEqual(0, [_modelContainer count]);
}


-(void) testReturnContentAtIndex
{
    [_modelContainer addObject: [[Model alloc] init]];
    Model* model = [_modelContainer objectAtIndex: 0];
    Model* modelToCompare = [[Model alloc] init];
    
    XCTAssertTrue([model isEqual: modelToCompare]);
}

-(void) testClearAllContent
{
    [_modelContainer clear];
    
    XCTAssertEqual([_modelContainer count], 0);
}

-(void) testInitWithJsonDictionary
{
    NSDictionary* film = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"James Bond", @"Title",
                          @"none", @"Genre",
                          @"none", @"Director",
                          @"none", @"Writer",
                          @"2012", @"Year",
                          @"NA", @"Poster",
                          @"NA", @"Plot",
                          @"9", @"Metascore",
                          @"9", @"imdbRaiting",
                          @"NA", @"Type",
                          @"NA", @"Actors",
                          nil];
    
    NSDictionary* dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                [[NSArray alloc] initWithObjects: film, nil],
                                @"Search",
                                nil];
    ModelContainer* model = [[ModelContainer alloc] initWithJsonDictionary: dictionary];
    
    XCTAssertEqual([model count], 1);
}

-(void) testInitWithEmptyJsonDictionary
{
    NSDictionary* dictionary = [[NSDictionary alloc] init];
    ModelContainer* model = [[ModelContainer alloc] initWithJsonDictionary: dictionary];
    
    XCTAssertEqual([model count], 1);
}

@end
