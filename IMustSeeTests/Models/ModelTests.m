//
//  IMustSeeTests.m
//  IMustSeeTests
//
//  Created by Bartłomiej Parowicz on 31.10.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Model.h"

@interface ModelTests: XCTestCase

@property Model* model;

@end

@implementation ModelTests

- (void)setUp
{
    [super setUp];
    _model = [[Model alloc] initWithTitle: @"James Bond"
                                    genre: @"action"
                                 director: @"none"
                                   writer: @"none"
                                     year: @"2012"
                                   poster: @"NA"
                                     plot: @"NA"
                                metascore: @"9"
                               imdbRating: @"9"
                                     type: @"NA"
                                   actors: @"NA"
                                   imdbID: @"NA"];
}

- (void)tearDown
{
    [super tearDown];
}

- (void) testInitWithJsonDictionary
{
    NSDictionary* dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                @"James Bond", @"Title",
                                @"action", @"Genre",
                                @"none", @"Director",
                                @"none", @"Writer",
                                @"2012", @"Year",
                                @"NA", @"Poster",
                                @"NA", @"Plot",
                                @"9", @"Metascore",
                                @"9", @"imdbRating",
                                @"NA", @"Type",
                                @"NA", @"Actors",
                                @"NA", @"imdbID",
                                nil];
    
    Model* modelFromDic = [[Model alloc] initWithJsonDictionary: dictionary];
    
    XCTAssertTrue([_model isEqual: modelFromDic]);
}

-(void) testInitWithEmptyJsonDictionary
{
    NSDictionary* dictionary = [[NSDictionary alloc] init];
    Model* modelFromDic = [[Model alloc] initWithJsonDictionary: dictionary];
    Model* emptyModel = [[Model alloc] init];
    
    XCTAssertTrue([modelFromDic isEqual: emptyModel]);
}

- (void)testCopy
{
    Model* copy = [_model copy];
    
    XCTAssertTrue([_model isEqual: copy]);
}

- (void)testEqual
{
    Model* emptyModel = [[Model alloc] init];
    Model* sameModel = [[Model alloc] initWithTitle: @"James Bond"
                                              genre: @"action"
                                           director: @"none"
                                             writer: @"none"
                                               year: @"2012"
                                             poster: @"NA"
                                               plot: @"NA"
                                          metascore: @"9"
                                         imdbRating: @"9"
                                               type: @"NA"
                                             actors: @"NA"
                                             imdbID: @"NA"];
    
    XCTAssertFalse([_model isEqual: emptyModel]);
    XCTAssertTrue([_model isEqual: sameModel]);
}

@end
