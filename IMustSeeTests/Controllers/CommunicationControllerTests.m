//
//  CommunicationController.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 23.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DictionaryExtension.h"

@interface DictionaryExtensionTests : XCTestCase

@property NSDictionary* dictionary;

@end

@implementation DictionaryExtensionTests

- (void)setUp
{
    [super setUp];
    
    _dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                   @"James Bond", @"Title",
                   [[NSNull alloc] init], @"Genre",
                   @"none", @"Director",
                   @"", @"Writer",
                   @"2012", @"Year",
                   @"NA", @"Poster",
                   @"NA", @"Plot",
                   @"9", @"Metascore",
                   @"9", @"ImdbRaiting",
                   @"NA", @"Type",
                   @"NA", @"Actors",
                   nil];
    
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testShouldGetValueFromKey
{
    NSString* value = [_dictionary objectForKey: @"Title" defaultValue: nil];
    
    XCTAssertEqual(@"James Bond", value);
}

- (void)testShouldGetEmptyStringFromKeyWhenValueIsNSNull
{
    NSString* value = [_dictionary objectForKey: @"Genre" defaultValue: @""];
    
    XCTAssertEqual([value length], 0);
}

- (void)testShouldGetNilWhenKeyIsWrong
{
    NSString* value = [_dictionary objectForKey: @"NonExistingKey" defaultValue: nil];
    
    XCTAssertNil(value);
}

@end
