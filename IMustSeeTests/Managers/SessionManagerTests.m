//
//  SessionManagerTests.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 14.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "SessionManager.h"
#import "ModelContainer.h"
#import "Model.h"

@interface SessionManagerTests : XCTestCase

@property SessionManager* sessionManager;
@property ModelContainer* modelContainer;

@end

@implementation SessionManagerTests

- (void)setUp
{
    [super setUp];
    
    Model* emptyFilm = [[Model alloc] init];
    Model* jamesBondFilm = [[Model alloc] initWithTitle: @"James Bond"
                                                  genre: @"action"
                                               director: @"none"
                                                 writer: @"none"
                                                   year: @"2012"
                                                 poster: @"NA"
                                                   plot: @"NA"
                                              metascore: @"9"
                                             imdbRating: @"9"
                                                   type: @"NA"
                                                 actors: @"NA"
                                                 imdbID: @"NA"];

    _modelContainer = [[ModelContainer alloc] init];
    [_modelContainer addObject: emptyFilm];
    [_modelContainer addObject: jamesBondFilm];
    
    _sessionManager = [SessionManager sharedInstance];

}

- (void)tearDown
{
    [super tearDown];
}

- (void)testStoreDataToDisk
{
    [_sessionManager store: _modelContainer];
    NSString* path = [_sessionManager pathToSession];
    
    XCTAssertTrue([[NSFileManager defaultManager] fileExistsAtPath: path isDirectory: nil]);
}

- (void)testRestoreDataFromDisk
{
    [_sessionManager store: _modelContainer];
    ModelContainer* restoredContainer = [_sessionManager restore];
    
    XCTAssertTrue([_modelContainer isEqualToArray: restoredContainer]);
}

- (void) testRemoveStoredDataFileFromDisk
{
    [_sessionManager store: _modelContainer];
    [_sessionManager clear];
    NSString* path = [_sessionManager pathToSession];
    
    XCTAssertFalse([[NSFileManager defaultManager] fileExistsAtPath: path isDirectory: nil]);
}

@end
