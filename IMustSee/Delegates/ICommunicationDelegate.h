//
//  ICommunicationDelegate.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

typedef void (^Handler)(NSData* data, NSURLResponse* response, NSError* error);

@protocol ICommunicationDelegate <NSObject>

-(void) send: (NSString*) request completionHandler: (Handler) handler;

@end