//
//  IListenerDelegate.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 23.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

@class ModelContainer;

@protocol IListenerDelegate <NSObject>

-(void) didRecive: (ModelContainer*) data;
-(void) didReciveError: (NSString*) error;

@end