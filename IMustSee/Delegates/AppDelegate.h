//
//  AppDelegate.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 31.10.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

