//
//  CommunicationDelegate.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OmdbCommunicationDelegate.h"
#import "CommunicationManager.h"
#import "PlistHelper.h"

@interface OmdbCommunicationDelegate()

-(NSURL*) buildUrlFromQuery: (NSString*) searchQuery;
-(NSString*) convertSearchTypeToNSString;
-(NSString*) convertPlotToNSString;
-(NSString*) convertContentToNSString;

@property NSURLSessionConfiguration* sessionConfig;
@property NSURLSession* session;
@property NSString* stringUrl;

@end

@implementation OmdbCommunicationDelegate : NSObject

-(id) init
{
    self = [super init];
    
    if (self)
    {
        _searchType = ImdbSearch;
        _plotType = ImdbShort;
        _sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session = [NSURLSession sessionWithConfiguration: _sessionConfig
                                                 delegate: nil
                                            delegateQueue: nil];
    }
    
    return self;
}

-(void) send: (NSString*) request completionHandler: (Handler) handler
{
    NSURL* url = [self buildUrlFromQuery: request];
    NSURLSessionDataTask* jsonData = [_session dataTaskWithURL: url
                                             completionHandler: handler];
    
    [jsonData resume];
}

-(NSURL*) buildUrlFromQuery: (NSString*) searchQuery
{
    NSString* stringUrl = [PlistHelper getUrlById: @"Omdb"];
    
    stringUrl = [stringUrl stringByReplacingOccurrencesOfString: @"searchQuery"
                                                     withString: searchQuery];
    
    stringUrl = [stringUrl stringByReplacingOccurrencesOfString: @"searchType"
                                                     withString: [self convertSearchTypeToNSString]];
    
    stringUrl = [stringUrl stringByReplacingOccurrencesOfString: @"plotType"
                                                     withString: [self convertPlotToNSString]];
    
    stringUrl = [stringUrl stringByReplacingOccurrencesOfString: @"contentType"
                                                     withString: [self convertContentToNSString]];
    
    return [[NSURL alloc] initWithString: stringUrl];
}

-(NSString*) convertSearchTypeToNSString
{
    NSString* result = @"";
    
    switch (_searchType)
    {
        case ImdbTitle:
            result = @"t";
            break;
        case ImdbSearch:
            result = @"s";
            break;
        default:
            break;
    }
    
    
    return result;
}

-(NSString*) convertPlotToNSString
{
    NSString* result = @"";
    
    switch (_plotType)
    {
        case ImdbFull:
            result = @"full";
            break;
        case ImdbShort:
            result = @"short";
            break;
        default:
            break;
    }
    
    return result;
}

-(NSString*) convertContentToNSString
{
    NSString* result = @"";
    
    switch (_contentType)
    {
        case ImdbMovie:
            result = @"movie";
            break;
        case ImdbSeries:
            result = @"series";
            break;
        case ImdbEpisode:
            result = @"episode";
            break;
        default:
            break;
    }
    
    return result;
}

@end