//
//  IModelContainerDelegate.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

@class Model;

@protocol IModelContainerDelegate <NSObject>

-(void) addModel: (Model*) model;

@end