//
//  ImdbCommunicationDelegate.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import "ICommunicationDelegate.h"

enum ImdbSearchType
{
    ImdbTitle,
    ImdbSearch
};

enum ImdbContentType
{
    ImdbNone,
    ImdbMovie,
    ImdbSeries,
    ImdbEpisode
};

enum ImdbPlotType
{
    ImdbFull,
    ImdbShort
};

@interface OmdbCommunicationDelegate: NSObject<ICommunicationDelegate>

-(id) init;
-(void) send: (NSString*) request completionHandler: (Handler) handler;

@property enum ImdbSearchType searchType;
@property enum ImdbPlotType plotType;
@property enum ImdbContentType contentType;

@end
