//
//  Film.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 05.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import "ISerialization.h"

@interface Model : NSObject<NSCopying, NSCoding, ISerialization>

-(id) init;
-(id) initWithCoder: (NSCoder*)aDecoder;
-(id) initWithJsonDictionary: (NSDictionary*) data;
-(id) initWithTitle: (NSString*) title
              genre: (NSString*) genre
           director: (NSString*) director
             writer: (NSString*) writer
               year: (NSString*) year
             poster: (NSString*) poster
               plot: (NSString*) plot
          metascore: (NSString*) metascore
         imdbRating: (NSString*) imdbRating
               type: (NSString*) type
             actors: (NSString*) actors
             imdbID: (NSString*) imdbID;

-(id) copyWithZone:(NSZone *)zone;
-(void) encodeWithCoder:(NSCoder *)aCoder;
-(BOOL) isEqual: (Model*) film;

@property NSString* title;
@property NSString* genre;
@property NSString* director;
@property NSString* writer;
@property NSString* year;
@property NSString* poster;
@property NSString* plot;
@property NSString* metascore;
@property NSString* imdbRating;
@property NSString* type;
@property NSString* actors;
@property NSString* imdbID;

@end
