//
//  DataModel.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 05.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelContainer.h"
#import "Model.h"

@interface ModelContainer()

@property NSMutableArray<Model*>* models;

@end

@implementation ModelContainer

-(id) init
{
    self = [super init];
    
    if (self)
    {
        _models = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        _models = [aDecoder decodeObjectForKey: @"Films"];
    }

    return self;
}

-(id) initWithJsonDictionary: (NSDictionary*) jsonData
{
    self = [super init];
    
    if (self)
    {
        _models = [[NSMutableArray alloc] init];
        NSArray* jsonMovies = [jsonData valueForKey: @"Search"];
        
        if (jsonMovies)
        {
            for (NSDictionary* entry in jsonMovies)
            {
                [_models addObject: [[Model alloc] initWithJsonDictionary: entry]];
            }
        }
        else
        {
            [_models addObject: [[Model alloc] initWithJsonDictionary: jsonData]];
        }
    }
    
    return self;
}

-(id) copyWithZone:(NSZone *)zone
{
    ModelContainer* copy = [[ModelContainer alloc] init];
    
    if (copy)
    {
        copy.models = [self.models copyWithZone: zone];
    }
    
    return copy;
}

-(void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject: _models forKey: @"Films"];
}

-(void) addObject: (id) object
{
    BOOL result = FALSE;
    
    for (Model* model in _models)
    {
        if ([model isEqual: object])
        {
            result = TRUE;
        }
    }
    
    if (!result && object)
    {
        [_models addObject: object];
    }
}

-(void) removeObjectAtIndex: (NSUInteger) index
{
    if (index < [self count])
    {
        [_models removeObjectAtIndex: index];
    }
}

-(void) clear
{
    [_models removeAllObjects];
}

-(Model*) getFilmAtIndex: (NSUInteger) index
{
    return [_models objectAtIndex: index];
}

-(NSUInteger) count
{
    return [_models count];
}

-(id) objectAtIndex: (NSUInteger) index
{
    Model* model = nil;
    
    if (index < [self count])
    {
        model = [_models objectAtIndex: index];
    }
    
    return model;
}

-(BOOL) isEqualToArray: (ModelContainer*) modelContainer
{
    return [_models isEqualToArray: modelContainer.models];
}

@end