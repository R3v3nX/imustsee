//
//  Film.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 05.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DictionaryExtension.h"
#import "Model.h"

@implementation Model

-(id) init
{
    return [self initWithTitle: @""
                         genre: @""
                      director: @""
                        writer: @""
                          year: @""
                        poster: @""
                          plot: @""
                     metascore: @""
                    imdbRating: @""
                          type: @""
                        actors: @""
                        imdbID: @""];
}

-(id) initWithCoder:(NSCoder*)aDecoder
{
    return [self initWithTitle: [aDecoder decodeObjectForKey: @"Title"]
                         genre: [aDecoder decodeObjectForKey: @"Genre"]
                      director: [aDecoder decodeObjectForKey: @"Director"]
                        writer: [aDecoder decodeObjectForKey: @"Writer"]
                          year: [aDecoder decodeObjectForKey: @"Year"]
                        poster: [aDecoder decodeObjectForKey: @"Poster"]
                          plot: [aDecoder decodeObjectForKey: @"Plot"]
                     metascore: [aDecoder decodeObjectForKey: @"Metascore"]
                    imdbRating: [aDecoder decodeObjectForKey: @"imdbRating"]
                          type: [aDecoder decodeObjectForKey: @"Type"]
                        actors: [aDecoder decodeObjectForKey: @"Actors"]
                        imdbID: [aDecoder decodeObjectForKey: @"imdbID"]];
}

-(id) initWithJsonDictionary: (NSDictionary*) dictionary
{
    return [self initWithTitle: [dictionary objectForKey: @"Title" defaultValue: @""]
                         genre: [dictionary objectForKey: @"Genre" defaultValue: @""]
                      director: [dictionary objectForKey: @"Director" defaultValue: @""]
                        writer: [dictionary objectForKey: @"Writer" defaultValue: @""]
                          year: [dictionary objectForKey: @"Year" defaultValue: @""]
                        poster: [dictionary objectForKey: @"Poster" defaultValue: @""]
                          plot: [dictionary objectForKey: @"Plot" defaultValue: @""]
                     metascore: [dictionary objectForKey: @"Metascore" defaultValue: @""]
                    imdbRating: [dictionary objectForKey: @"imdbRating" defaultValue: @""]
                          type: [dictionary objectForKey: @"Type" defaultValue: @""]
                        actors: [dictionary objectForKey: @"Actors" defaultValue: @""]
                        imdbID: [dictionary objectForKey: @"imdbID" defaultValue: @""]];
}

-(id) initWithTitle: (NSString *)title
              genre: (NSString *)genre
           director: (NSString *)director
             writer: (NSString *)writer
               year: (NSString *)year
             poster: (NSString *)poster
               plot: (NSString *)plot
          metascore: (NSString *)metascore
         imdbRating: (NSString *)imdbRating
               type: (NSString *)type
             actors: (NSString *)actors
             imdbID: (NSString *)imdbID
{
    self = [super init];
    
    if (self)
    {
        _title = title;
        _genre = genre;
        _director = director;
        _writer = writer;
        _year = year;
        _poster = poster;
        _plot = plot;
        _metascore = metascore;
        _imdbRating = imdbRating;
        _type = type;
        _actors = actors;
        _imdbID = imdbID;
    }
    
    return self;
}

-(id) copyWithZone:(NSZone *)zone
{
    Model* copy = [[Model allocWithZone: zone] init];
    
    if (copy)
    {
        copy.title = self.title;
        copy.genre = self.genre;
        copy.director = self.director;
        copy.writer = self.writer;
        copy.year = self.year;
        copy.poster = self.poster;
        copy.plot = self.plot;
        copy.metascore = self.metascore;
        copy.imdbRating = self.imdbRating;
        copy.type = self.type;
        copy.actors = self.actors;
        copy.imdbID = self.imdbID;
    }
    
    return copy;
}

-(void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject: _title forKey: @"Title"];
    [aCoder encodeObject: _genre forKey: @"Genre"];
    [aCoder encodeObject: _director forKey: @"Director"];
    [aCoder encodeObject: _writer forKey: @"Writer"];
    [aCoder encodeObject: _year forKey: @"Year"];
    [aCoder encodeObject: _poster forKey: @"Poster"];
    [aCoder encodeObject: _plot forKey: @"Plot"];
    [aCoder encodeObject: _metascore forKey: @"Metascore"];
    [aCoder encodeObject: _imdbRating forKey: @"imdbRating"];
    [aCoder encodeObject: _type forKey: @"Type"];
    [aCoder encodeObject: _actors forKey: @"Actors"];
    [aCoder encodeObject: _imdbID forKey: @"imdbID"];
}

-(BOOL) isEqual: (Model *)film
{
    return  [_title isEqualToString: film.title] &&
            [_genre isEqualToString: film.genre] &&
            [_director isEqualToString: film.director] &&
            [_writer isEqualToString: film.writer] &&
            [_year isEqualToString: film.year] &&
            [_poster isEqualToString: film.poster] &&
            [_plot isEqualToString: film.plot] &&
            [_metascore isEqualToString: film.metascore] &&
            [_imdbRating isEqualToString: film.imdbRating] &&
            [_type isEqualToString: film.type] &&
            [_actors isEqualToString: film.actors] &&
            [_imdbID isEqualToString: film.imdbID];
}

@end