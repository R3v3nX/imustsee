//
//  DataModel.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 05.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import "ISerialization.h"

@interface ModelContainer : NSObject<NSCopying, NSCoding, ISerialization>

-(id) init;
-(id) initWithCoder:(NSCoder *)aDecoder;
-(id) initWithJsonDictionary: (NSDictionary*) jsonData;

-(id) copyWithZone:(NSZone *)zone;
-(id) objectAtIndex: (NSUInteger) index;

-(void) encodeWithCoder:(NSCoder *)aCoder;
-(void) addObject: (id) object;
-(void) removeObjectAtIndex: (NSUInteger) index;
-(void) clear;

-(NSUInteger) count;
-(BOOL) isEqualToArray: (ModelContainer*) dataModel;

@end

