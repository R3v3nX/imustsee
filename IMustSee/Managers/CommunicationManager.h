//
//  CommunicationController.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IListenerDelegate;
@protocol ICommunicationDelegate;

enum CMSearchType
{
    CMByTitle,
    CMBySearch
};

enum CMFilter
{
    CMNone,
    CMMovie,
    CMEpisode,
    CMSeries
};

@interface CommunicationManager : NSObject

+(id) sharedInstance;

-(void) searchFor: (NSString*) query searchType: (enum CMSearchType) type withFilter: (enum CMFilter) filter;
-(void) searchFor: (NSString*) query searchType:(enum CMSearchType)type withFilter:(enum CMFilter)filter andDelay: (float) delay;

@property id<ICommunicationDelegate> communicationDelegate;
@property id<IListenerDelegate> listenerDelegate;

@end

