//
//  SessionManager.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 14.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "ModelContainer.h"

@interface SessionManager()

-(id) init;

@end

@implementation SessionManager

+(id) sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^(void)
    {
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(id) init
{
    self = [super init];
    
    if (self)
    {
        NSArray<NSURL*>* documentsUrl = [[NSFileManager defaultManager]
                                         URLsForDirectory: NSDocumentDirectory
                                                inDomains: NSUserDomainMask];
        
        NSURL* urlAbsolutePath = [[documentsUrl firstObject]
                                  URLByAppendingPathComponent: @"films.data"];
        
        _pathToSession = [urlAbsolutePath path];
    }
    
    return self;
}

-(void) store: (id) modelContainer
{
    BOOL archiveStatus = [NSKeyedArchiver archiveRootObject: modelContainer
                                                     toFile: _pathToSession];
    
    if (!archiveStatus)
    {
        NSLog(@"%s, can't store model to file!", __PRETTY_FUNCTION__);
    }
}

-(id) restore
{
    id modelContainer = [NSKeyedUnarchiver unarchiveObjectWithFile: _pathToSession];
    
    if (!modelContainer)
    {
        modelContainer = [[ModelContainer alloc] init];
        NSLog(@"%s, can't restore model from file!", __PRETTY_FUNCTION__);
    }
    
    return modelContainer;
}

-(void) clear
{
    NSError* error;
    [[NSFileManager defaultManager] removeItemAtPath: _pathToSession error: &error];
    
    if (error)
    {
        NSLog(@"%s, %@", __PRETTY_FUNCTION__, [[error userInfo] valueForKey: @"NSLocalizedDescription"]);
    }
}

-(NSString*) getPathToSessionFile
{
    return _pathToSession;
}

@end