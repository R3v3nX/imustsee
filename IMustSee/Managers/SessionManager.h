//
//  SessionManager.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 14.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

@interface SessionManager : NSObject

+(id) sharedInstance;

-(id) restore;
-(void) store: (id) model;
-(void) clear;

@property (readonly) NSString* pathToSession;

@end
