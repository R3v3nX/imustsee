//
//  CommunicationController.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "IListenerDelegate.h"
#import "OmdbCommunicationDelegate.h"
#import "ICommunicationDelegate.h"
#import "CommunicationManager.h"
#import "Serializer.h"
#import "ModelContainer.h"

@interface  CommunicationManager()

-(id) init;

-(void) configureImdbDelegateWithSearchType: (enum CMSearchType) type andFilter: (enum CMFilter) filter;
-(void) sendQuery: (NSTimer*) arg;

-(enum ImdbContentType) convertFilterToImdbContent: (enum CMFilter) filter;
-(NSString*) formatQuery: (NSString*) text;

@property Serializer* serializer;
@property NSTimer* timer;

@end

@implementation CommunicationManager

+(id) sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^(void)
    {
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(id) init
{
    self = [super init];
    
    if (self)
    {
        _communicationDelegate = [[OmdbCommunicationDelegate alloc] init];
        _serializer = [[Serializer alloc] init];
        _listenerDelegate = nil;
    }
    
    return self;
}

-(void) searchFor: (NSString*) query searchType:(enum CMSearchType)type withFilter:(enum CMFilter)filter
{
    [self searchFor: query searchType:type withFilter: filter andDelay: 0.f];
}

-(void) searchFor: (NSString*) query searchType:(enum CMSearchType)type withFilter:(enum CMFilter)filter andDelay: (float) delay
{
    [self configureImdbDelegateWithSearchType: type andFilter: filter];
    NSString* formatedQuery = [self formatQuery: query];
    
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval: delay
                                              target: self
                                            selector: @selector(sendQuery:)
                                            userInfo: formatedQuery
                                             repeats: FALSE];
    
    [_timer fire];
}

-(void) sendQuery: (NSTimer*) arg
{
    [_communicationDelegate send: [arg userInfo]
               completionHandler: ^(NSData *data, NSURLResponse *response, NSError* error)
     {
         dispatch_sync(dispatch_get_main_queue(), ^(void)
         {
             if (!error)
             {
                 ModelContainer* model = [_serializer deserialize: data];
                 [_listenerDelegate didRecive: model];
             }
             else
             {
                 NSLog(@"%s %@", __PRETTY_FUNCTION__, [error description]);
                 [_listenerDelegate didReciveError: [[error userInfo] objectForKey: @"NSLocalizedDescription"]];
             }
         });
     }];
}

-(void) configureImdbDelegateWithSearchType: (enum CMSearchType) type andFilter: (enum CMFilter)filter
{
    OmdbCommunicationDelegate* delegate = (OmdbCommunicationDelegate*)_communicationDelegate;
    
    delegate.searchType = (type == CMByTitle) ? ImdbTitle : ImdbSearch;
    delegate.plotType = (type == CMByTitle) ? ImdbFull: ImdbShort;
    delegate.contentType = [self convertFilterToImdbContent: filter];
}

-(enum ImdbContentType) convertFilterToImdbContent: (enum CMFilter) filter
{
    enum ImdbContentType result;
    
    switch (filter)
    {
        case CMNone:
            result = ImdbNone;
            break;
        case CMMovie:
            result = ImdbMovie;
            break;
        case CMEpisode:
            result = ImdbEpisode;
            break;
        case CMSeries:
            result = ImdbSeries;
            break;
        default:
            break;
    }
    
    return result;
}

-(NSString*) formatQuery: (NSString*) text
{
    NSMutableString* formatedQuery = [[NSMutableString alloc] initWithString: [text stringByReplacingOccurrencesOfString: @" "
                                                                                                              withString: @"+"]];
    if ([formatedQuery hasSuffix: @"+"])
    {
        [formatedQuery deleteCharactersInRange: NSMakeRange([formatedQuery length]-1, 1)];
    }
    
    return formatedQuery;
}

@end