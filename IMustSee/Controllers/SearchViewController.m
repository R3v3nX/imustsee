//
//  SearchTableViewController.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 29.01.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "SearchViewController.h"
#import "DetailViewController.h"

#import "OmdbCommunicationDelegate.h"
#import "IModelContainerDelegate.h"

#import "CommunicationManager.h"
#import "SearchCellView.h"

#import "ModelContainer.h"
#import "Model.h"
#import "UICustomAlert.h"

@interface SearchViewController()

-(void) initSearchController;
-(void) initTableView;
-(void) initCommunicationManager;
-(void) initWaitingIndicator;

-(BOOL) isEmptyContent: (Model*) data;
-(void) searchQuery: (UISearchBar *)searchBar withDelay: (float) delay;
-(enum CMFilter) convertToCMFilter: (NSString*) filter;
-(IBAction) onBackButtonPush:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy) ModelContainer* dataModel;
@property CommunicationManager* communicationManager;
@property UISearchController* searchController;
@property UIActivityIndicatorView* waitingIndicator;

@end

@implementation SearchViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    [self initCommunicationManager];
    [self initSearchController];
    [self initTableView];
    [self initWaitingIndicator];
    
    _dataModel = nil;
    self.definesPresentationContext = TRUE;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self initCommunicationManager];
}

-(void) initSearchController
{
    _searchController = [[UISearchController alloc] initWithSearchResultsController: nil];
    _searchController.searchResultsUpdater = self;
    _searchController.searchBar.delegate = self;
    _searchController.searchBar.scopeButtonTitles = @[@"All", @"Movie", @"Episode", @"Series"];
    _searchController.searchBar.barTintColor = self.navigationController.navigationBar.barTintColor;
    _searchController.dimsBackgroundDuringPresentation = FALSE;
}

-(void) initTableView
{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = _searchController.searchBar;
}

-(void) initCommunicationManager
{
    _communicationManager = [CommunicationManager sharedInstance];
    _communicationManager.listenerDelegate = self;
}

-(void) initWaitingIndicator
{
    _waitingIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    
    _waitingIndicator.center = self.view.center;
    [self.view addSubview: _waitingIndicator];
}

-(void) dealloc
{
    [_searchController loadViewIfNeeded];
}

-(void) didRecive: (ModelContainer*) data;
{
    [_waitingIndicator stopAnimating];
    
    _dataModel = ([self isEmptyContent: [data objectAtIndex: 0]]) ? nil : data;
    
    [_tableView reloadData];
}

-(BOOL) isEmptyContent: (Model*) data
{
    return data && [data isEqual: [[Model alloc] init]];
}

-(void) didReciveError: (NSString*) errorMessage
{
    [_waitingIndicator stopAnimating];
    _searchController.searchBar.text = @"";
    [_searchController.searchBar resignFirstResponder];
    
    UICustomAlert* alert = [[UICustomAlert alloc] initWithAction: nil title: @"Error" andErrorMessage: errorMessage];
    [alert showWithShakeAnimationOn: self];
}

-(void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    NSInteger noOfRowsInSection = [_dataModel count];
    _tableView.separatorStyle = (noOfRowsInSection == 0) ? UITableViewCellSeparatorStyleNone
                                                         : UITableViewCellSeparatorStyleSingleLine;

    return noOfRowsInSection;
}

-(UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*)indexPath
{
    SearchCellView* cell = [tableView dequeueReusableCellWithIdentifier: @"SearchCellView"];
    Model* model = [_dataModel objectAtIndex: indexPath.row ];
    
    if (model)
    {
        cell.title.text = [@"Title: " stringByAppendingString: model.title];
        cell.type.text = [@"Type: " stringByAppendingString: model.type];
        cell.year.text = [@"Year: " stringByAppendingString: model.year];
        [cell.poster sd_setImageWithURL: [NSURL URLWithString: model.poster]
                       placeholderImage: [UIImage imageNamed:@"UIImagePlaceholder.png"]];
    }
    
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction* more = [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault
                                                                    title: @"More"
                                                                  handler: ^(UITableViewRowAction* action, NSIndexPath* indexPath)
    {
        
        [_tableView reloadRowsAtIndexPaths: @[indexPath]
                          withRowAnimation: UITableViewRowAnimationRight];
        [self performSegueWithIdentifier: @"segueToDetailView" sender: indexPath];

    }];
    
    UITableViewRowAction* add = [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault
                                                                   title: @"Add"
                                                                 handler: ^(UITableViewRowAction* action, NSIndexPath* indexPath)
    {
        [_modelContainerDelegate addModel: [_dataModel objectAtIndex: indexPath.row]];
        [_tableView reloadRowsAtIndexPaths: @[indexPath]
                          withRowAnimation: UITableViewRowAnimationRight];
    }];
    
    add.backgroundColor = [UIColor greenColor];
    more.backgroundColor = [UIColor lightGrayColor];
    
    return @[add, more];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier: @"segueToDetailView" sender: indexPath];
}

-(void) prepareForSegue: (UIStoryboardSegue*) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString: @"segueToDetailView"])
    {
        UINavigationController* naviCtrl = (UINavigationController*) segue.destinationViewController;
        Model* model = [_dataModel objectAtIndex: [(NSIndexPath*)sender row]];
        SearchCellView* cell = [self.tableView cellForRowAtIndexPath: (NSIndexPath*)sender];
        
        if (model && cell)
        {
            DetailViewController* detailViewCtrl = (DetailViewController*)naviCtrl.topViewController;
            detailViewCtrl.modelTitle = model.title;
            detailViewCtrl.modelImage = cell.poster.image;
            detailViewCtrl.modelContainerDelegate = _modelContainerDelegate;
        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchQuery: searchBar withDelay: 0.f];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self searchQuery: searchController.searchBar withDelay: 1.f];
}

-(void) searchQuery: (UISearchBar *)searchBar withDelay: (float) delay
{
    if ([searchBar.text length] != 0)
    {
        [_waitingIndicator startAnimating];
        
        NSString* filter = [searchBar.scopeButtonTitles objectAtIndex: searchBar.selectedScopeButtonIndex];
        enum CMFilter cmFilter = [self convertToCMFilter: filter];
        
        [_communicationManager searchFor: searchBar.text searchType: CMBySearch withFilter: cmFilter andDelay: delay];
    }
}

-(enum CMFilter) convertToCMFilter: (NSString*) filter
{
    enum CMFilter result = CMNone;
    
    if ([filter isEqualToString: @"Movie"])
    {
        result = CMMovie;
    }
    else if ([filter isEqualToString: @"Episode"])
    {
        result = CMEpisode;
    }
    else if ([filter isEqualToString: @"Series"])
    {
        result = CMSeries;
    }
    
    return result;
}

-(IBAction) onBackButtonPush:(id)sender
{
    [self dismissViewControllerAnimated: TRUE completion: nil];
}

@end