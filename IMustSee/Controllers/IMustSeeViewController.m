//
//  RankingViewController.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 12.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "IMustSeeViewController.h"
#import "DetailViewController.h"
#import "SearchViewController.h"

#import "SessionManager.h"
#import "ModelContainer.h"
#import "Model.h"

#import "IMustSeeCellView.h"

@interface IMustSeeViewController()

-(void) initTableViewDelegates;
-(void) storeModelContainer;
-(void) restoreModelContainer;
-(void) segueToDetailView: (UIStoryboardSegue*) segue sender: (id) sender;
-(void) segueToSearchView: (UIStoryboardSegue*) segue sender: (id) sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property ModelContainer* modelContainer;

@end

@implementation IMustSeeViewController

-(id) initWithCoder: (NSCoder*) coder
{
    self = [super initWithCoder:coder];
    
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(storeModelContainer)
                                                     name: UIApplicationDidEnterBackgroundNotification
                                                   object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(storeModelContainer)
                                                     name: UIApplicationWillTerminateNotification
                                                   object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(restoreModelContainer)
                                                     name: UIApplicationWillEnterForegroundNotification
                                                   object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(restoreModelContainer)
                                                     name: UIApplicationDidBecomeActiveNotification
                                                   object: nil];
    }
    
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self initTableViewDelegates];
}

-(void) initTableViewDelegates
{
    _tableView.delegate = self;
    _tableView.dataSource = self;
}

-(void) storeModelContainer;
{
    [[SessionManager sharedInstance] store: _modelContainer];
}

-(void) restoreModelContainer
{
    _modelContainer = [[SessionManager sharedInstance] restore];
}

-(void) addModel: (Model*) model
{
    [_modelContainer addObject: model];
    [_tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRowsInSection =  [_modelContainer count];
    
    _label.hidden = (noOfRowsInSection == 0) ? NO : YES;
    _tableView.scrollEnabled = (noOfRowsInSection == 0) ? NO : YES;
    _tableView.separatorStyle = (noOfRowsInSection == 0) ? UITableViewCellSeparatorStyleNone
                                                         : UITableViewCellStyleDefault;
    _tableView.backgroundView = _label;
    
    return noOfRowsInSection;
}

-(UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*)indexPath
{
    IMustSeeCellView* cell = [tableView dequeueReusableCellWithIdentifier: @"IMustSeeCellView"];
    Model* model = [_modelContainer objectAtIndex: indexPath.row ];
    
    if (model)
    {
        cell.title.text = [@"Title: " stringByAppendingString: model.title];
        cell.type.text = [@"Type: " stringByAppendingString: model.type];
        cell.year.text = [@"Year: " stringByAppendingString: model.year];
        [cell.poster sd_setImageWithURL: [NSURL URLWithString: model.poster]
                       placeholderImage: [UIImage imageNamed: @"UIImagePlaceholder.png"]];
    }
    
    return cell;
}
-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction* more = [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault
                                                                    title: @"More"
                                                                  handler: ^(UITableViewRowAction* action, NSIndexPath* indexPath)
    {
        [_tableView reloadRowsAtIndexPaths: @[indexPath]
                          withRowAnimation: UITableViewRowAnimationRight];
        [self performSegueWithIdentifier: @"segueToDetailView" sender: indexPath];
    }];
    
    UITableViewRowAction* delete = [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault
                                                                      title: @"Delete"
                                                                    handler: ^(UITableViewRowAction* action, NSIndexPath* indexPath)
    {
        [_modelContainer removeObjectAtIndex: indexPath.row];
        [_tableView deleteRowsAtIndexPaths: @[indexPath]
                          withRowAnimation: UITableViewRowAnimationFade];
    }];
    
    delete.backgroundColor = [UIColor redColor];
    more.backgroundColor = [UIColor lightGrayColor];
    
    return @[delete, more];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier: @"segueToDetailView" sender: indexPath];
}

-(void) prepareForSegue: (UIStoryboardSegue*) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString: @"segueToDetailView"])
    {
        [self segueToDetailView: segue sender: sender];
    }
    else if ([segue.identifier isEqualToString: @"segueToSearchView"])
    {
        [self segueToSearchView: segue sender: sender];
    }
}

-(void) segueToDetailView: (UIStoryboardSegue*) segue sender: (id) sender
{
    UINavigationController* naviCtrl = (UINavigationController*) segue.destinationViewController;
    Model* model = [_modelContainer objectAtIndex: [(NSIndexPath*)sender row]];
    IMustSeeCellView* cell = [self.tableView cellForRowAtIndexPath: (NSIndexPath*)sender];
    
    if (cell)
    {
        DetailViewController* detailViewCtrl = (DetailViewController*) naviCtrl.topViewController;
        detailViewCtrl.modelTitle = model.title;
        detailViewCtrl.modelImage = cell.poster.image;
        detailViewCtrl.modelContainerDelegate = self;
    }
}

-(void) segueToSearchView: (UIStoryboardSegue*) segue sender: (id) sender
{
    UINavigationController* naviCtrl = (UINavigationController*) segue.destinationViewController;
    SearchViewController* searchViewCtrl = (SearchViewController*) naviCtrl.topViewController;
    searchViewCtrl.modelContainerDelegate = self;
}

@end