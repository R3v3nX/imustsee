//
//  SearchViewController.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 29.01.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IListenerDelegate.h"

@protocol IModelContainerDelegate;

@interface  SearchViewController: UIViewController<IListenerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate>

-(void) didRecive: (ModelContainer*) data;
-(void) didReciveError: (NSString*) errorMessage;

@property id<IModelContainerDelegate> modelContainerDelegate;

@end

