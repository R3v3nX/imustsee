//
//  DetailView.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 01.02.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IListenerDelegate.h"

@protocol IModelContainerDelegate;

@interface DetailViewController: UIViewController<IListenerDelegate>

-(void) didRecive: (ModelContainer*) data;
-(void) didReciveError: (NSString*) errorMessage;

@property UIImage* modelImage;
@property NSString* modelTitle;
@property id<IModelContainerDelegate> modelContainerDelegate;

@end