//
//  DetailView.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 01.02.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import "DetailViewController.h"
#import "CommunicationManager.h"

#import "OmdbCommunicationDelegate.h"
#import "IModelContainerDelegate.h"

#import "ModelContainer.h"
#import "Model.h"

#import "UICustomAlert.h"
#import "PlistHelper.h"

@interface DetailViewController()

-(void) initCommunicationManager;
-(void) initFBLikeButton;
-(void) initWaitingIndicator;

-(void) setContent;
-(void) setHeightOfContent;
-(void) setFBButtonsContent;

@property (weak, nonatomic) IBOutlet UIImageView *poster;
@property (weak, nonatomic) IBOutlet UILabel *filmTitle;
@property (weak, nonatomic) IBOutlet UILabel *genre;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *imdbRating;
@property (weak, nonatomic) IBOutlet UILabel *metascore;
@property (weak, nonatomic) IBOutlet UILabel *director;
@property (weak, nonatomic) IBOutlet UILabel *writer;
@property (weak, nonatomic) IBOutlet UILabel *actors;
@property (weak, nonatomic) IBOutlet UILabel *plot;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (weak, nonatomic) IBOutlet UIView *detailContentView;
@property (weak, nonatomic) IBOutlet UIView *detailLikeView;

@property CommunicationManager* communicationManager;
@property FBSDKLikeControl* fbLikeControl;
@property UIActivityIndicatorView* waitingIndicator;
@property Model* model;

@end

@implementation DetailViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self initCommunicationManager];
    [self initFBLikeButton];
    [self initWaitingIndicator];
    
    [_detailContentView setHidden: TRUE];
    _model = nil;
}

-(void) initCommunicationManager
{
    _communicationManager = [CommunicationManager sharedInstance];
    _communicationManager.listenerDelegate = self;
    [_communicationManager searchFor: _modelTitle
                          searchType: CMByTitle
                          withFilter: CMNone];
}

-(void) initFBLikeButton
{
    _fbLikeControl = [[FBSDKLikeControl alloc] init];
    _fbLikeControl.likeControlStyle = FBSDKLikeControlStyleBoxCount;
    [_detailLikeView addSubview: _fbLikeControl];
}

-(void) initWaitingIndicator
{
    _waitingIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    
    _waitingIndicator.center = self.view.center;
    [self.view addSubview: _waitingIndicator];
    [_waitingIndicator startAnimating];
}

-(void) didRecive: (ModelContainer*) data
{
    _model = (Model*)[data objectAtIndex: 0];
    
    [self setContent];
    [self setHeightOfContent];
    [self setFBButtonsContent];
    
    [_waitingIndicator stopAnimating];
    [_detailContentView setHidden: FALSE];
    
    [self.view setNeedsDisplay];
}

-(void) setContent
{
    _filmTitle.text = [@"Title: " stringByAppendingString: _model.title];
    _director.text = [@"Director: " stringByAppendingString: _model.director];
    _genre.text = [@"Genre: " stringByAppendingString: _model.genre];
    _writer.text = [@"Writer: " stringByAppendingString: _model.writer];
    _year.text = [@"Year: " stringByAppendingString: _model.year];
    _actors.text = [@"Actors: " stringByAppendingString: _model.actors];
    _metascore.text = [@"Metascore: " stringByAppendingString: _model.metascore];
    _imdbRating.text = [@"ImdbRating: " stringByAppendingString: _model.imdbRating];
    _plot.text = [@"Plot: " stringByAppendingString: _model.plot];
    _poster.image = _modelImage;
}

-(void) setHeightOfContent
{
    float height = 0;
    
    for (UIView* view in _detailContentView.subviews)
    {
        if ([view isKindOfClass: [UILabel class]])
        {
            [view sizeToFit];
        }
        
        height += view.frame.size.height;
    }
    
    _detailScrollView.contentSize = CGSizeMake(_detailScrollView.contentSize.width, height);
}

-(void) setFBButtonsContent
{
    NSString* imdbLink = [PlistHelper getUrlById: @"Imdb"];
    imdbLink = [imdbLink stringByReplacingOccurrencesOfString: @"imdbID"
                                                   withString: _model.imdbID];
    _fbLikeControl.objectID = imdbLink;
}

-(void) didReciveError: (NSString*) errorMessage
{
    [_waitingIndicator stopAnimating];
    
    UIAlertAction* action = [UIAlertAction actionWithTitle: @"OK"
                                                     style: UIAlertActionStyleDefault
                                                   handler: ^(UIAlertAction * action)
                             {
                                 [self dismissViewControllerAnimated:YES completion: nil];
                             }];
    
    UICustomAlert* alert = [[UICustomAlert alloc] initWithAction: action
                                                           title: @"Error"
                                                 andErrorMessage: errorMessage];
    [alert showWithShakeAnimationOn: self];
}

-(IBAction) onDoneButtonPush:(id)sender
{
    [self dismissViewControllerAnimated:YES completion: nil];
}

-(IBAction) onSaveButtonPush:(id)sender
{
    [_modelContainerDelegate addModel: _model];
    [self dismissViewControllerAnimated:YES completion: nil];
}

@end