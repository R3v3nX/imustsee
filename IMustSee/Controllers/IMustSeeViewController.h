//
//  RankingViewController.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 12.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IModelContainerDelegate.h"

@interface IMustSeeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, IModelContainerDelegate>

-(void) addModel: (Model*) model;

@end
