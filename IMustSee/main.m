//
//  main.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 31.10.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
