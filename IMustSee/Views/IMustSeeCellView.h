//
//  CellView.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 29.01.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMustSeeCellView : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* title;
@property (nonatomic, weak) IBOutlet UILabel* year;
@property (nonatomic, weak) IBOutlet UILabel* type;
@property (nonatomic, weak) IBOutlet UIImageView* poster;

@end
