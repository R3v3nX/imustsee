//
//  Serializer.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 07.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Serializer.h"
#import "ModelContainer.h"

@implementation Serializer

-(ModelContainer*) deserialize: (NSData*) data;
{
    ModelContainer* model = nil;
    NSError* jsonError = nil;
    NSDictionary* parsedJson = [NSJSONSerialization JSONObjectWithData: data
                                                               options: 0
                                                                 error: &jsonError];
    if (!jsonError)
    {
        model = [[ModelContainer alloc] initWithJsonDictionary: parsedJson];
    }
    else
    {
        NSLog(@"%s, %@",__PRETTY_FUNCTION__, [[jsonError userInfo] objectForKey: @"NSLocalizedDescription"]);
    }
    
    return model;
}

@end