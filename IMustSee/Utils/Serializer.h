//
//  Serializer.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 07.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

@class ModelContainer;

@interface Serializer : NSObject

-(ModelContainer*) deserialize: (NSData*) data;

@end
