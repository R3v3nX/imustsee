//
//  Alerts.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 11.07.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

@interface UICustomAlert : NSObject

-(id) init;
-(id) initWithAction: (UIAlertAction*) action
               title:(NSString*) title
         andErrorMessage:(NSString*) message;

-(void) showOn: (UIViewController*) controller;
-(void) showWithShakeAnimationOn: (UIViewController*) controller;

-(void) setTitle: (NSString *) title;
-(void) setMessage: (NSString*) message;

@end
