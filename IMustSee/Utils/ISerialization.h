//
//  ISerialization.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 21.12.2015.
//  Copyright © 2015 Bartłomiej Parowicz. All rights reserved.
//

@protocol ISerialization <NSObject>

@required
-(id) initWithJsonDictionary: (NSDictionary*) data;

@end
