//
//  PlistHelper.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 11.07.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

@interface PlistHelper : NSObject 

+(NSString*) getUrlById: (NSString*) urlId;

@end
