//
//  PlistHelper.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 11.07.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlistHelper.h"


@implementation PlistHelper

+(NSString*) getUrlById: (NSString*) urlId
{
    NSString* result = @"";
    NSArray* array = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"Supported URL"];

    for (NSDictionary* dic in array)
    {
        id valueFromDic = [dic objectForKey: urlId];
        
        if (valueFromDic != nil)
        {
            result = valueFromDic;
        }
    }
    
    return result;
}

@end