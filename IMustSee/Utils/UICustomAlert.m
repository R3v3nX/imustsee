//
//  Alert.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 11.07.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "UICustomAlert.h"

@interface UICustomAlert()

-(void) initShakeAnimation;

@property UIAlertController* alert;
@property CAKeyframeAnimation* shakeAnimation;

@end

@implementation UICustomAlert

-(id) init
{
    return [self initWithAction: nil title: @"" andErrorMessage: @""];
}

-(id) initWithAction: (UIAlertAction*) action
               title:(NSString*) title
     andErrorMessage:(NSString*) message
{
    self = [super self];
    
    if (self)
    {
        UIAlertAction* defaultAction = (action) ? action : [UIAlertAction actionWithTitle: @"OK"
                                                                                    style: UIAlertActionStyleDefault
                                                                                  handler: ^(UIAlertAction * action) {}];
        _alert = [UIAlertController alertControllerWithTitle: @""
                                                     message: @""
                                              preferredStyle: UIAlertControllerStyleAlert];
        

        _alert.title = title;
        _alert.message = message;
        [_alert addAction: defaultAction];
        
        [self initShakeAnimation];
    }
    
    return self;
}

-(void) initShakeAnimation
{
    _shakeAnimation = [[CAKeyframeAnimation alloc] init];
    _shakeAnimation = [CAKeyframeAnimation animation];
    _shakeAnimation.keyPath = @"position.x";
    _shakeAnimation.values = @[@0, @10, @-10, @10, @0];
    _shakeAnimation.keyTimes = @[@0.0, @(1.0/6.0), @(3.0/6.0), @(5.0/6.0), @1.0];
    _shakeAnimation.duration = 0.4;
    _shakeAnimation.additive = TRUE;
}

-(void) showOn: (UIViewController*) controller
{
    [controller presentViewController: _alert animated: YES completion: nil];
}

-(void) showWithShakeAnimationOn: (UIViewController*) controller
{
    [controller presentViewController: _alert animated: YES completion: nil];
    [controller.view.layer addAnimation: _shakeAnimation forKey: @"shake"];
}

-(void) setTitle: (NSString *)title
{
    _alert.title = title;
}

-(void) setMessage: (NSString*)message
{
    _alert.message = message;
}

@end
