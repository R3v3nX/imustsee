//
//  DictionaryHelper.h
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 08.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DictionaryExtension)

-(id) objectForKey:(NSString*) key defaultValue:(id) defaultValue;

@end
