//
//  DictionaryHelper.m
//  IMustSee
//
//  Created by Bartłomiej Parowicz on 08.06.2016.
//  Copyright © 2016 Bartłomiej Parowicz. All rights reserved.
//

#import "DictionaryExtension.h"

@implementation NSDictionary (DictionaryExtension)

-(id) objectForKey:(NSString*) key defaultValue:(id) defaultValue;
{
    id value = [self valueForKey: key];
    
    if (!value || value == [NSNull null])
    {
        value = defaultValue;
    }
    
    return value;
}

@end